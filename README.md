# Suitmedia

Suitmedia Mobile Developer Intern - Second Screening Test

Implemented: 

- Implement `isPalindrome` method to check if inserted **Name** on **Home Screen** is a palindrome or not. Method can be seen in [isPalindrome method file](lib/services/palindrome_check.dart) which takes in a **String** and returns a **Boolean** if the string is a palindrome or not.
- Added a method to check if the month of a guest's birthdate is prime or not. Method can be seen in [monthIsPrime method file](lib/services/month_is_prime.dart) which takes in a **DateTime** as an input and returns a **Boolean** if the month is prime or not. A dialog containing the guest's month of birthdate is prime or not will be shown after clicking on a guest.

- Enhance UI for **Home Screen**
- Enhance UI for **Events Screen**
- Create new horizontal snappable list of events and Maps after clicking the add button on the top right corner of **Events Screen**
- Add pull-to-refresh and Caching data in **Guests Screen**

## Screenshots of Implementation

**Bear in mind that some are GIFs and require some time to load.**

### Home Page

#### Initial State

<img src="README_images/home_enhanced.png" alt="home_with_name_enhanced" width="250" />

#### Filling the name form field

<img src="README_images/home_with_name_enhanced.png" alt="home_with_name_enhanced" width="250" />

#### Check if inserted name isPalindrome or not

To access the alert dialog, press the **Selesai** button after entering a name.

- Name: **kasur rusak**

  <img src="README_images/home_kasur.png" alt="kasur rusak" width="250" />

- Name: **step on no pets**

  <img src="README_images/home_step.png" alt="step on no pets" width="250" />

- Name: **put it up**

  <img src="README_images/home_putitup.png" alt="putitup" width="250" />

- Name: **suitmedia**

  <img src="README_images/home_suit.png" alt="suitmedia" width="250" />

### Pick Event and Pick Guest Screen

Accessed after clicking `Selesai` button on **Home Screen**.

#### Initial State

<img src="README_images/pickeventguest.png.png" alt="asd" width="250" />



### Event Screen

Accessed after click on the `Pilih Event` button on Pick Event and Pick Guest Screen. 

#### Initial State

*Apologies for the bad recording since this was recorded in an Emulator.

<img src="README_images/event_static_enh.png" width="250" />

<img src="README_images/event_dynamic_enh.gif" alt="dynamic event" width="250" />



#### Clicking the **Add article** button on the top right hand corner

<img src="README_images/maps_static.png" alt="maps static" width="250" />

<img src="README_images/maps_dynamic.gif" alt="maps dynamic" width="250" />

### Guest Screen

Can be accessed after clicking on the `Pilih Guest` button. Data taken from http://www.mocky.io/v2/596dec7f0f000023032b8017. Implemented 

#### Initial state

<img src="README_images/guest.png" width="250" />

#### Pull-to-refresh

<img src="README_images/guest_pulltorefresh.gif" alt="pulltorefresh" width="250" />

#### Bilangan Prima

<img src="README_images/prime.gif" alt="prime" width="250" />

import 'dart:math';

import 'package:intl/intl.dart';

bool monthIsPrime(DateTime date) {
  String monthStr = DateFormat('MM').format(date);
  int month = int.parse(monthStr);
  return numIsPrime(month);
}

bool numIsPrime(int num) {
  int factors = 0;
  for (int i = 1; i <= sqrt(num).ceil(); i++) {
    if (num % i == 0) factors++;
    if (factors > 2) return false;
  }
  return true;
}

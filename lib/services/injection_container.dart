import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_it/get_it.dart';

Future<void> getItInject() async {
  final cacheManager = DefaultCacheManager();
  GetIt.I.registerSingleton<CacheManager>(cacheManager);
}

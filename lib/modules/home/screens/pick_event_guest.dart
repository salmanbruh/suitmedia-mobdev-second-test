import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test/components/custom_button.dart';
import 'package:suitmedia_screening_test/modules/event/screens/event.dart';
import 'package:suitmedia_screening_test/modules/guest/screens/guest.dart';

class PickEventAndGuestScreen extends StatefulWidget {
  PickEventAndGuestScreen({
    this.insertedName,
    Key key,
  }) : super(key: key);

  final String insertedName;

  @override
  _PickEventAndGuestScreenState createState() =>
      _PickEventAndGuestScreenState();
}

class _PickEventAndGuestScreenState extends State<PickEventAndGuestScreen> {
  String _eventName = "";
  String _guestName = "";

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildNameSection(context),
              SizedBox(height: 24),
              _buildEventButton(context),
              SizedBox(height: 8),
              _buildGuestButton(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNameSection(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 5,
          child: Text(
            "Nama",
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Expanded(
          flex: 5,
          child: Text(
            ": " + widget.insertedName,
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
      ],
    );
  }

  Widget _buildEventButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: CustomButton(
        text: _eventName == "" ? Text("Pilih Event") : Text(_eventName),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => EventScreen()),
          ).then((value) => {if (value != null) _setEventName(value)});
        },
      ),
    );
  }

  Widget _buildGuestButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: CustomButton(
        text: _guestName == "" ? Text("Pilih Guest") : Text(_guestName),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => GuestScreen()),
          ).then((value) => {if (value != null) _setGuestName(value)});
        },
      ),
    );
  }

  void _setEventName(String newEventName) {
    setState(() {
      _eventName = newEventName;
    });
  }

  void _setGuestName(String newGuestName) {
    setState(() {
      _guestName = newGuestName;
    });
  }
}

import 'package:flutter/material.dart';

class Guest {
  Guest({this.id, this.name, this.birthdate, this.image});

  factory Guest.fromJson(Map<String, dynamic> json) {
    return Guest(
      id: json["id"],
      name: json["name"].toString(),
      birthdate: DateTime.parse(json["birthdate"].toString()),
      image: AssetImage('lib/assets/images/dummy_person.png'),
    );
  }

  int id;
  String name;
  DateTime birthdate;
  AssetImage image;
}

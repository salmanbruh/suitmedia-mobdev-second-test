import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:suitmedia_screening_test/modules/guest/models/guest.dart';
import 'package:suitmedia_screening_test/services/month_is_prime.dart';

class GuestScreen extends StatefulWidget {
  @override
  _GuestScreenState createState() => _GuestScreenState();
}

class _GuestScreenState extends State<GuestScreen> {
  List<Guest> _guestsList = [];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  CacheManager _cacheManager = GetIt.I();

  @override
  void initState() {
    _getGuestsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Center(
            child: Text("GUEST"),
          ),
        ),
        body: _guestsList.length == 0
            ? Center(child: CircularProgressIndicator())
            : _buildGuestGridView(context),
      ),
    );
  }

  Widget _buildGuestGridView(BuildContext context) {
    return SmartRefresher(
      child: GridView.count(
        crossAxisCount: 2,
        children: _guestsList.map((guest) => _buildGuestsCard(guest)).toList(),
      ),
      controller: _refreshController,
      enablePullDown: true,
      header: MaterialClassicHeader(),
      onRefresh: _getGuestsList,
    );
  }

  Widget _buildGuestsCard(Guest guest) {
    return InkWell(
      onTap: () {
        _showMonthIsPrimeDialog(context, guest);
      },
      child: Container(
        margin: EdgeInsets.all(16),
        child: Center(
          child: Text(
            guest.name,
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: guest.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildSnackBar(
    BuildContext context,
    Widget content,
  ) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }

  Future<dynamic> _showMonthIsPrimeDialog(BuildContext context, Guest guest) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        String guestBirthDate = DateFormat('dd-MM-yy').format(guest.birthdate);
        String guestBirthDateMonth = guestBirthDate.split('-')[1];
        return AlertDialog(
          content: RichText(
            text: TextSpan(
              style: TextStyle(color: Colors.black),
              children: [
                TextSpan(text: guest.name),
                TextSpan(text: " berulang tahun pada "),
                TextSpan(text: guestBirthDate),
                TextSpan(text: ". Bulan ke-"),
                TextSpan(text: guestBirthDateMonth),
                if (!monthIsPrime(guest.birthdate))
                  TextSpan(
                    text: " bukan",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                TextSpan(
                  text: " bilangan prima.",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Kembali"),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                String alertMessage = _getAlertMessage(guest.birthdate);
                _buildSnackBar(context, Text(alertMessage));
                Navigator.pop(context, guest.name);
              },
              child: Text("Lanjut"),
            ),
          ],
        );
      },
    );
  }

  String _getAlertMessage(DateTime birthDate) {
    int day = int.parse(DateFormat('dd').format(birthDate));

    if (day % 2 == 0) {
      if (day % 3 == 0) {
        return "iOS";
      } else {
        return "blackberry";
      }
    } else if (day % 3 == 0) {
      return "android";
    }

    return "Feature phone";
  }

  Future<void> _getGuestsList() async {
    _setGuestsList([]);

    var jsonFile = await _cacheManager
        .getSingleFile("https://www.mocky.io/v2/596dec7f0f000023032b8017");
    String jsonAsString = await jsonFile.readAsString();
    List<dynamic> jsonAsList = await json.decode(jsonAsString);

    List<Guest> guestsList = [];
    jsonAsList.forEach((guestJson) {
      Guest guest = Guest.fromJson(guestJson);
      guestsList.add(guest);
    });

    _setGuestsList(guestsList);

    _refreshController.refreshCompleted();
  }

  void _setGuestsList(List<Guest> newGuestsList) {
    setState(() {
      _guestsList = newGuestsList;
    });
  }
}

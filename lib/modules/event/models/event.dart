import 'package:google_maps_flutter/google_maps_flutter.dart';

class Event {
  Event({
    this.name,
    this.datetime,
    this.description,
    this.tags,
    this.coordinate,
    this.imageUrl,
  });

  String name;
  DateTime datetime;
  String description;
  List<String> tags;
  LatLng coordinate;
  String imageUrl;
}

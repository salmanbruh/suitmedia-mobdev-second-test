import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:lorem_ipsum/lorem_ipsum.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';
import 'package:suitmedia_screening_test/modules/event/models/event.dart';
import 'package:suitmedia_screening_test/services/random_datetime_generator.dart';

class EventScreen extends StatefulWidget {
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  Random random = Random();
  List<Event> _eventsList = [];
  bool _isEventsListScreen = true;
  Completer<GoogleMapController> _mapController = Completer();
  LatLng _initialCameraPosition = LatLng(-6.200000, 106.816666);
  Map<String, Marker> _markers = {};

  @override
  void initState() {
    _eventsList = _getEventsList();
    _initialCameraPosition = _eventsList[0].coordinate;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: _buildAppBar(context),
        body: _isEventsListScreen
            ? _buildEventsListView(context)
            : _buildEventsMap(context),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      leading: InkWell(
        child: Image.asset(
          "lib/assets/modules/events/btn_backArticle_normal.png",
          color: Colors.teal,
        ),
        onTap: () {
          Navigator.pop(context);
        },
      ),
      title: Text(
        "MESSAGE FROM CODI",
        style: TextStyle(
          color: Colors.teal,
        ),
      ),
      backgroundColor: Colors.white,
      shadowColor: Colors.teal,
      bottom: PreferredSize(
        child: Container(
          color: Colors.teal,
          height: 2,
        ),
        preferredSize: Size.fromHeight(2),
      ),
      actions: [
        IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.teal,
          ),
          onPressed: () {},
        ),
        InkWell(
          child: Image.asset(
            "lib/assets/modules/events/btn_newMediaArticle_normal.png",
            width: 20,
            height: 20,
            color: Colors.teal,
          ),
          onTap: () {
            _changeScreen();
          },
        ),
        SizedBox(width: 16),
      ],
    );
  }

  Widget _buildEventsListView(BuildContext context) {
    return ListView.separated(
      itemCount: _eventsList.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: _buildEventCard(context, _eventsList[index]),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(),
    );
  }

  Widget _buildEventCard(BuildContext context, Event event) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.pop(context, event.name);
        },
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 7,
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  margin: EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        event.name,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(height: 4),
                      Text(
                        DateFormat('MMM dd y').format(event.datetime),
                        style: TextStyle(
                          color: Colors.orangeAccent,
                        ),
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: event.tags
                            .map((tag) => _buildEventTag(tag))
                            .toList(),
                      ),
                      SizedBox(height: 8),
                      Flexible(
                        child: Text(
                          event.description,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 4,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Image.network(event.imageUrl + "200/300"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildEventTag(String tagLabel) {
    return Container(
      padding: EdgeInsets.all(4),
      margin: EdgeInsets.fromLTRB(0, 0, 4, 0),
      child: Text(
        tagLabel,
        style: TextStyle(
          color: Colors.white,
          fontSize: 10,
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.blueGrey[200],
      ),
    );
  }

  Widget _buildEventsMap(BuildContext context) {
    _getMarkers();
    return Column(
      children: [
        Expanded(
          child: ScrollSnapList(
            scrollDirection: Axis.horizontal,
            itemCount: _eventsList.length,
            itemSize: 248,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                child: _buildEventsHorizontalScrollCard(
                  context,
                  _eventsList[index],
                ),
              );
            },
            onItemFocus: (int index) {
              _onSnap(_eventsList[index]);
            },
          ),
        ),
        Expanded(
          flex: 3,
          child: GoogleMap(
            initialCameraPosition: CameraPosition(
              target: _initialCameraPosition,
              zoom: 10,
            ),
            onMapCreated: (GoogleMapController controller) {
              _mapController.complete(controller);
            },
            myLocationEnabled: true,
            myLocationButtonEnabled: true,
            markers: _markers.values.toSet(),
          ),
        )
      ],
    );
  }

  Widget _buildEventsHorizontalScrollCard(BuildContext context, Event event) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12, horizontal: 24),
      width: 200,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.all(4),
            child: Text(
              event.name,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(event.imageUrl + "200/600"),
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  void _changeScreen() {
    setState(() {
      _isEventsListScreen = !_isEventsListScreen;
    });
  }

  String _getRandomImageUrl() {
    int imgId = random.nextInt(200);
    String imgUrl = "https://picsum.photos/id/$imgId/";
    return imgUrl;
  }

  List<Event> _getEventsList() {
    return List.generate(
      10,
      (index) => Event(
        name: "Event " + (index + 1).toString(),
        datetime: generateRandomDateTime(2000, 2050),
        description: loremIpsum(words: 50),
        tags: ["Tag 1", "Tag 2"],
        coordinate: _getRandomCoordinate(),
        imageUrl: _getRandomImageUrl(),
      ),
    );
  }

  LatLng _getRandomCoordinate() {
    var minLat = -6.128052585891995;
    var minLong = 106.69527053815578;

    var randomLat = minLat - random.nextDouble();
    var randomLong = minLong + random.nextDouble();
    return LatLng(randomLat, randomLong);
  }

  Future<void> _goToMarker(CameraUpdate cameraUpdate) async {
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(cameraUpdate);
  }

  void _getMarkers() async {
    _markers.clear();

    _eventsList.forEach((event) {
      final marker = Marker(
        markerId: MarkerId(event.name),
        position: event.coordinate,
        infoWindow: InfoWindow(title: event.name),
      );
      _markers[event.name] = marker;
    });
  }

  void _onSnap(Event event) {
    var newPosition = CameraPosition(
      target: event.coordinate,
      zoom: 10,
    );

    CameraUpdate update = CameraUpdate.newCameraPosition(newPosition);

    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }

    _goToMarker(update);
  }
}
